#!/bin/bash -x

apt-get update
apt-get install -y --no-install-recommends apt-utils dialog whiptail
apt-get install -y sudo
if [ 0 = $(grep -c 'wheel' /etc/sudoers) ]; then
  chmod +w /etc/sudoers
  echo '%wheel ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
  chmod -w /etc/sudoers
fi
