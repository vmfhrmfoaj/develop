#!/bin/bash -x

function rc_file() {
  echo "${HOME}/.$(basename $(getent passwd | grep ^${SUDO_USER} | head -1 | awk -F: '{print $NF}'))rc"
}

# install system packages
apt-get install -y curl git-core gnupg locales zsh vim wget unzip

# setup
locale-gen 'en_US.UTF-8'
touch /etc/hosts
if [ 0 = $(grep -c "$hostname" /etc/hosts) ]; then
  echo "127.0.0.1  localhost $hostname" >> /etc/hosts
  echo "::1        localhost $hostname" >> /etc/hosts
fi
touch ~/.bashrc
if [ 0 = $(grep -c 'en_US.UTF-8' ~/.bashrc) ]; then
  echo 'export LANG=en_US.UTF-8'   >> ~/.bashrc
  echo 'export LC_ALL=en_US.UTF-8' >> ~/.bashrc
fi
touch /etc/sudoers
