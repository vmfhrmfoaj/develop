#!/bin/bash -x

cd "$(dirname $0)"

BOOTSTAP_ROOT=$(readlink -f ..)
MIRROR='http://mirror.kakao.com/ubuntu/'

target="$1"
ver="${2:-bionic}"

if [ ! -z "${target}" ]; then
  tmp="${BOOTSTAP_ROOT}/.cache/${ver}"
  cache="${BOOTSTAP_ROOT}/.cache/${ver}.tar.gz"
  mkdir -p "${tmp}"
  mkdir -p "${target}"
  mkdir -p $(dirname "${cache}")
  if [ ! -f "${cache}" ]; then
    debootstrap --variant=buildd --arch=amd64 "${ver}" "${tmp}" "${MIRROR}"
    if [ 0 -eq $? ]; then
      tar -czf "${cache}" -C "${tmp}" .
      rm -rf "${tmp}"
    fi
  fi
  if [ -f ${cache} ]; then
    tar -xf "${cache}" -C "${target}"
    rm -rf "${target}/bootstrap"
    cp -r "${BOOTSTAP_ROOT}/.common/" "${target}/bootstrap/"
    echo "deb ${MIRROR} ${ver} main universe" > "${target}/etc/apt/sources.list"
    echo "deb-src ${MIRROR} ${ver} main"     >> "${target}/etc/apt/sources.list"
  else
    return 1
  fi
fi
